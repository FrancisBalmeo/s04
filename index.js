/*Demo*/
document.getElementById("btn_1").addEventListener('click', ()=> {
	alert("Add More!");
});

let paragraph = document.getElementById("paragraph-1");
let paragraph2 = document.getElementById("paragraph-2");

document.getElementById("btn_2").addEventListener('click', ()=>{
	paragraph.innerHTML = "I can even do this!";
})



document.getElementById("btn_3").addEventListener('click', ()=>{
	paragraph2.innerHTML = "Or this!";
	paragraph2.style.color = "purple";
})


/*Lesson Proper
	Topics 
		- Introduction
		- Writing Comments
		- Syntax and Statements
		- Storing Values through variables*
		- Peeking at Variable's value through console
		- Data Types
		- Functions

Javascript - is a scripting language that enables yout to make 
interactive web pages
		   - it is one core technologies of the world wide web
		   - was originally intended for web browsers. However, they are 
		   now also integreated in web servers through the use of node.js

Uses of JavaScript
-Web app development
-broswer games
-art creation
-mobile application

External Javascript
*/

// Writing Comments in Java Script:

	// Two ways of writing:
	//Single Line comments - ctrl +/
	/*
		multi-line comments - ctrl + shift + /
		comments in JS, much like css and html, is not read by the broswer, so this comments are often used to add notes and to  add markers to your code

	*/
console.log("Hello, my name is Francis. I purple you.");
	
/* We can see or log messages in our console.

	- Consoles are part of our browser which will allow
	us to see/log messages, data or information from our programming language.

	- for most browsers, console can be accessed through its developer tools in the console tab

	in fact, consoles allow us to add some javascript expressions



Statements
	- are instructions, expressions we add to our programming language
	which will then be communicated to our computers
	- commonly ends in semi-colon(;) However, javascript has an implemented way  
	of automatically adding semicolons at the end of our statement. 
	Which means, unlike other languages, JS does not require semicolons.

	Semicolons are used to mark the end of the statement
Syntax
 	- is a set of rules that describes how statements are properly made/constructed

	-lines/block of codes must follow certain rules for it to work. REMEMBER: you are merely communicating with another human but with a computer.

*/

console.log("Francis"); //logs messages on the console.

//Variables
/*

In Html, elements are containers of other elment and text.
In JS, variables are containers of data. A given name is used to describe a piece of data.

Variables also allow us to use or refer to data multiple times

*/

let num = 10;//num is our variable and 10 is our data

console.log(6);
console.log(num);

let name = "Rin";
console.log("V");
console.log(name);

/*Creating Vairables




To Create a Variable, there are two steps to be done:
	-Declaration which actually allows to create the variable
	-Initialization which allows to add an initial value to a variable

Variables in JS are declared with the use of let or const keyword.

*/

let myVariable;
/*
We can create variables without an initial values.
However, when logged in the console, the variable will return a value od undefined.


Undefined is a datatype that indicates that variable exist but has no initial value.


*/

console.log(myVariable);
myVariable = "New Initialized Value";
console.log(myVariable);


/*
myVar = "Hello"
let myVar;
console.log(myVar);

Note: You should not access a variable before it's been created/declared


-Undefined vs Not Defined-

Undefined means a variable has been declared but has no initial values
Not Defined means means the variable you are trying to access does not exist

undefined - data type
not defined - an error

Note: Some errors in JS, will stop the program from further execution
*/

let myVariable3 = "Another text";
console.log(myVariable3);

/* Variables must be declared first before they are used, referred to, or accessed
*/



//Let vs Const

/*
Let - can create variables that can be declared, initialized, and re-assigned.
*/

let bestFinalFantasy;
bestFinalFantasy = "Final Fantasy VI";
console.log(bestFinalFantasy);

//re-assigning
bestFinalFantasy = "Final Fantasy VII";
console.log(bestFinalFantasy);


const pi = 3.1416;
console.log(pi);


/*
Const variables are variables with constant data. We should not re-declare constant values.


*/

const mvp = "Michael Jordan";
console.log(mvp);

/*Guides on Variable Names

	1. When naming variables, it is important to create variables that are descriptive and indicative of data it contains.
	let firstName = "RM"; - good var name
	let pokemon = 25000;  - bad var name


	2. When naming variables, it is better to start with a lowercase. We usually avoid creating variable names that start with capital letters, because there are reserved keywords that start in capital letter

	3. Do not add spaces to your variable names. Use camelCase or underscore for multiple words


*/


let num_sum = 5000;
let numSum = 6000;

console.log(num_sum);
console.log(numSum);


// Declaring mutiple values
let brand, model, type;
brand = "Toyota";
model = "Vios";
type = "Sedan";

console.log(brand);
console.log(model);
console.log(type);

//one line
console.log(brand, model, type);


//DATA TYPES!

/*

string literal = '', "" & ``
object literal = {}
array literal = []

*/

//Strings
	// are a series of alphanumeric that create a word or  phrase, or anything related to creating texts
	//use '' (single quotes) or "" (double quotes) to write strings

let country = "Philippines";
let province = 'Cavite';

console.log(country);
console.log(province);

/*

Mini-Activity:
	Create 2 variables named firstname and lastname
	Add your first name as strings to its appropriate variables
	log your first name and last name variables in the console at the same time
*/

let firstname, lastname;

firstname = "John Francis";
lastname = "Balmeo";

console.log(firstname, lastname);

/*Concatenation is process/operation wherein we combine two strings as one*/

console.log(firstname + " " + lastname);

let fullname = firstname + " " + lastname;

console.log(fullname);

let word1 = "is";
let word2 = "student";
let word3 = "of";
let word4 = "University";
let word5 = "De La Salle";
let word6 = "a";
let word7 = "Dasmarinas";
let space = " ";

/*
Mini-Activity
create a variable called sentence.

combine the variables to form a single string which would ligibly and understandably create " a student of DLSUD"

log the sentence variable in the console.
*/


let sentence = fullname + space + word1 + space + word6 + space + word2 + space + word3 + space + word5 + space + word4 + space + word7;
console.log(sentence);

/*let sentence = `${fullname} ${word1} ${word6} ${word2} ${word3} ${word5} ${word4} ${word7}`;
console.log(sentence);*/

/* Template Literal (``) will allow us to create string with the use of backticks. It also allow us to easily concatenate strings without the use of + (plus)
	It also allows to embed or add varaible and even expressions in our string with the use placeholders ${}.
*/

//Number (Data Type)
	//Integers (Whole Numbers) and floats (decimals). These are our number data which can be used for mathematical operations

	let numString1 = "5";
	let numString2 = "6";
	let num1 =5;
	let num2 =6;


	console.log(numString1 + numString2);
	console.log (num1 + num2);

	let num3 = 5.5;
	let num4 = .5;
	console.log(num3 + num4);
// the plus operator adds numbers while concatenates strings


//parseInt() - this can change the type of numeric string to a proper number 
console.log(parseInt(numString1) + num1);
